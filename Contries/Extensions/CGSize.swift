//
//  CGSize.swift
//  Contries
//
//  Created by Mohsen Moghimi on 1/21/1402 AP.
//

import UIKit

extension CGSize {
    var inverse: CGSize {
        .init(width: -1 * width, height: -1 * height)
    }
}
