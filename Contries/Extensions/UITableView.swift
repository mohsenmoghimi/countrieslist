//
//  UITableView.swift
//  Contries
//
//  Created by Mohsen Moghimi on 1/22/1402 AP.
//

import UIKit

extension UITableView {
    func setEmptyMessage(_ message: String) {
        let container = UIView()
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(systemName: "flag")
        imageView.tintColor = .lightGray
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.alignment = .center
        stackView.distribution = .fillEqually
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .lightGray
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.boldSystemFont(ofSize: 14)
        messageLabel.sizeToFit()
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(messageLabel)
        container.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.centerYAnchor.constraint(equalTo: container.centerYAnchor, constant: 0).isActive = true
        stackView.centerXAnchor.constraint(equalTo: container.centerXAnchor, constant: 0).isActive = true
        self.backgroundView = container
        self.separatorStyle = .none
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}

