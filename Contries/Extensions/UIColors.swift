//
//  UIColors.swift
//  Contries
//
//  Created by Mohsen Moghimi on 1/20/1402 AP.
//

import UIKit

extension UIColor {
    open class var primaryColor: UIColor {
        UIColor(named: "primaryColor")!
    }
}
