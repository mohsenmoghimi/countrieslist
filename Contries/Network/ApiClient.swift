//
//  ApiClient.swift
//  Contries
//
//  Created by Mohsen Moghimi on 1/20/1402 AP.
//

import Foundation
import Combine

typealias Promise<T> = Future<T, ClientError>
typealias Single<T> = AnyPublisher<T, ClientError>

protocol UrlRequestConvertor {
    func asURLRequest() throws -> URLRequest
}

protocol ApiClient {
    func request<T>(_: UrlRequestConvertor) -> Promise<T> where T : Codable
}
