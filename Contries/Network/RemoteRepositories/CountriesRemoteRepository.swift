//
//  CountriesRemoteRepository.swift
//  Contries
//
//  Created by Mohsen Moghimi on 1/20/1402 AP.
//

import Foundation

struct CountriesRemoteRepository {
    let client: ApiClient
    
    init(client: ApiClient) {
        self.client = client
    }
    
    func getCountries() -> Promise<[Country]> {
        return client.request(Router.getCountries)
    }
}


extension CountriesRemoteRepository {
    private enum Router: NetworkRouter {
        case getCountries
        
        var method: RequestMethod {
            switch self {
            case .getCountries:
                return .get
            }
        }
        
        var path: String {
            switch self {
            case .getCountries:
                return "v3.1/all"
            }
        }
        
        var params: [String : Any] {
            switch self {
            default:
                return [:]
            }
        }
        
    }
}
