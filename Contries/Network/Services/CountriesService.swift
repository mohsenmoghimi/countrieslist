//
//  CountriesService.swift
//  Contries
//
//  Created by Mohsen Moghimi on 1/20/1402 AP.
//

import Foundation
struct CountriesService {
    private let remote: CountriesRemoteRepository
    init(remote: CountriesRemoteRepository) {
        self.remote = remote
    }
    
    func getCountries() -> Single<[Country]> {
        return remote.getCountries().eraseToAnyPublisher()
    }
}

