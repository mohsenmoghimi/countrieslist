//
//  CountriesViewModel.swift
//  Contries
//
//  Created by Mohsen Moghimi on 1/21/1402 AP.
//

import Foundation
import Combine

final class CountriesViewModel {
    let container: DIContainer
    var cancellables = Set<AnyCancellable>()
    var isLoading = CurrentValueSubject<Bool, Never>(false)
    var error = PassthroughSubject<String, Never>()
    var dataSource = CurrentValueSubject<[Country], Never>([])
    var allCountries: [Country] = []
    var selectedCountries = CurrentValueSubject<[Country], Never>([])
    init(container: DIContainer) {
        self.container = container
    }
    
    /**
     Fetch all countries from remote API.
     */
    func getCountries() {
        isLoading.send(true)
        container.services.countriesService.getCountries()
            .sinkToResult { [weak self] result in
            switch result {
            case .success(let value):
                self?.saveObjects(objects: value)
                self?.dataSource.send(self?.bulkSelectCountries(on: value) ?? [])
                self?.allCountries = self?.bulkSelectCountries(on: value) ?? []
                self?.isLoading.send(false)
            case .failure(let err):
                self?.isLoading.send(false)
                self?.error.send(err.localizedDescription)
            }
        }.store(in: &cancellables)
    }
    
    /**
     Search in datasource and filter it.
     
     - Parameters:
     - text that specify search text.
     */
    func search(text: String) {
        if text.isEmpty {
            dataSource.send(allCountries)
        } else {
            let filtered = allCountries.filter({
                ($0.name?.official?.lowercased().range(of: text.lowercased()) != nil) ||
                ($0.name?.common?.lowercased().range(of: text.lowercased()) != nil) ||
                ($0.region?.lowercased().range(of: text.lowercased()) != nil)
            })
            dataSource.send(filtered)
        }
    }
    
    /**
     Save all fetched object in the user default.
     
     - Parameters:
     - objects that specify array of object that needs to be saved.
     */
    func saveObjects(objects: [Country]) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(objects){
            UserDefaults.standard.set(encoded, forKey: "user_countries")
            UserDefaults.standard.synchronize()
        }
    }
    
    /**
     Retrive all fetche countries on local memory.
     */
    func getLocalCountries() -> [Country] {
        if let objects = UserDefaults.standard.value(forKey: "user_countries") as? Data {
            let decoder = JSONDecoder()
            if let objectsDecoded = try? decoder.decode([Country].self, from: objects) as [Country] {
                return bulkSelectCountries(on: objectsDecoded)
            } else {
                return []
            }
        } else {
            return []
        }
    }
    
    /**
     Modify datasource to select a Country object.
     
     - Parameters:
     - item that needs to be select.
     */
    func selectCountry(item: Country) {
        if let index = allCountries.firstIndex(
            where: {$0.name?.official == item.name?.official}
        ) {
            allCountries[index].isSelected = true
        }
        if let index = dataSource.value.firstIndex(
            where: {$0.name?.official == item.name?.official}
        ) {
            dataSource.value[index].isSelected = true
        }
    }
    
    /**
     Modify datasource to deselect a Country object.
     
     - Parameters:
     - item that needs to be deselect.
     */
    func deselectCountry(item: Country) {
        if let index = allCountries.firstIndex(
            where: {$0.name?.official == item.name?.official}
        ) {
            allCountries[index].isSelected = false
        }
        if let index = dataSource.value.firstIndex(
            where: {$0.name?.official == item.name?.official}
        ) {
            dataSource.value[index].isSelected = false
        }
    }
    
    /**
     Pass selected countries to HomeViewCountroller.
     */
    func applySelectedCountries() {
        let selectedCountries = allCountries.filter { $0.isSelected == true }
        self.selectedCountries.send(selectedCountries)
    }
    
    /**
     Check the selecetd items datasource and select them in main datasource.
    
     - Parameters:
     - data datasource that needs to be modify.
     */
    func bulkSelectCountries(on data: [Country]) -> [Country] {
        let selectedCountries = self.selectedCountries.value
        var draft = data
        guard selectedCountries.count > 0 else { return data}
        for item in selectedCountries {
            if let index = draft.firstIndex(
                where: {$0.name?.official?.lowercased() == item.name?.official?.lowercased()}
            ) {
                draft[index].isSelected = true
            }
        }
        return draft
    }
}
