//
//  CountriesViewController.swift
//  Contries
//
//  Created by Mohsen Moghimi on 1/21/1402 AP.
//

import UIKit

class CountriesViewController: UIViewController {
    var viewModel: CountriesViewModel!
    
    // MARK: UI Emlements
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(reloadList), for: .valueChanged)
        refreshControl.tintColor = .primaryColor
        return refreshControl
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: view.bounds)
        tableView.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        tableView.separatorStyle = .none
        tableView.refreshControl = refreshControl
        tableView.allowsMultipleSelection = true
        tableView.keyboardDismissMode = .onDrag
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()
    
    private lazy var loadingContainer: UIView = {
        let container = UIView()
        container.backgroundColor = .gray
        container.alpha = 0.6
        container.layer.cornerRadius = 20
        container.isHidden = true
        return container
    }()
    
    private lazy var loadingIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.style = .medium
        indicator.color = .white
        indicator.startAnimating()
        return indicator
    }()
    
    private lazy var searchController: UISearchController = {
        let searchController = UISearchController()
        searchController.searchBar.delegate = self
        searchController.searchBar.showsBookmarkButton = true
        searchController.searchBar.barTintColor = .primaryColor
        searchController.searchBar.searchTextField.backgroundColor = .white
        searchController.searchBar.searchTextField.textColor = .primaryColor
        searchController.searchBar.searchTextField.leftView?.tintColor = .primaryColor
        searchController.searchBar.setImage(UIImage(systemName: "magnifyingglass"), for: .bookmark, state: .normal)
        searchController.searchBar.searchTextField.attributedPlaceholder = NSAttributedString(string:"Search on countries", attributes:[NSAttributedString.Key.foregroundColor: UIColor.primaryColor])
        return searchController
    }()
    
    // MARK: View lifcycles
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Countries"
        view.backgroundColor = .primaryColor
        navigationItem.searchController = searchController
        setupView()
        perepareData()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneTapped))
    }
    
    // MARK: Constraint views
    private func constraintTableView() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(
            equalTo: view.topAnchor,
            constant: 0
        ).isActive = true
        tableView.bottomAnchor.constraint(
            equalTo: view.bottomAnchor,
            constant: 0
        ).isActive = true
        tableView.leadingAnchor.constraint(
            equalTo: view.leadingAnchor,
            constant: 0
        ).isActive = true
        tableView.trailingAnchor.constraint(
            equalTo: view.trailingAnchor,
            constant: 0
        ).isActive = true
    }
    
    private func constraintLoadingContainer() {
        view.addSubview(loadingContainer)
        loadingContainer.translatesAutoresizingMaskIntoConstraints = false
        loadingContainer.centerXAnchor.constraint(
            equalTo: view.centerXAnchor,
            constant: 0
        ).isActive = true
        loadingContainer.centerYAnchor.constraint(
            equalTo: view.centerYAnchor,
            constant: 0
        ).isActive = true
        loadingContainer.widthAnchor.constraint(
            equalToConstant: 100
        ).isActive = true
        loadingContainer.heightAnchor.constraint(
            equalToConstant: 100
        ).isActive = true
    }
    
    private func constraintLoadingIndicator() {
        loadingContainer.addSubview(loadingIndicator)
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
        loadingIndicator.centerXAnchor.constraint(
            equalTo: loadingContainer.centerXAnchor,
            constant: 0
        ).isActive = true
        loadingIndicator.centerYAnchor.constraint(
            equalTo: loadingContainer.centerYAnchor,
            constant: 0
        ).isActive = true
    }
    
    // MARK: Subscriptios
    private func dataSourceSubscription() {
        viewModel.dataSource
            .receive(on: DispatchQueue.main)
            .sink {[weak self] _ in
                self?.refreshControl.endRefreshing()
                self?.tableView.reloadData()
        }.store(in: &viewModel.cancellables)
    }
    
    private func loadingSubscription() {
        viewModel.isLoading
            .receive(on: DispatchQueue.main)
            .sink {[weak self] isLoading in
                self?.loadingContainer.isHidden = !isLoading
        }.store(in: &viewModel.cancellables)
    }
    
    // MARK: Methods
    private func setupView() {
        constraintTableView()
        constraintLoadingContainer()
        constraintLoadingIndicator()
    }
    
    private func perepareData() {
        dataSourceSubscription()
        loadingSubscription()
        if viewModel.getLocalCountries().count != 0 {
            viewModel.allCountries = viewModel.getLocalCountries()
            viewModel.dataSource.send(viewModel.getLocalCountries())
        } else {
            viewModel.getCountries()
        }
    }
    
    @objc private func reloadList() {
        viewModel.search(text: "")
        viewModel.getCountries()
    }
    
    @objc private func doneTapped() {
        viewModel.applySelectedCountries()
        navigationController?.popViewController(animated: true)
    }
}

// MARK: UITableViewDelegate & UITableViewDataSource
extension CountriesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel.dataSource.value[indexPath.row]
        let cell = CountryTableViewCell()
        cell.config(item: item)
        if item.isSelected ?? false {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        } else {
            tableView.deselectRow(at: indexPath, animated: false)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataSource.value.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = viewModel.dataSource.value[indexPath.row]
        viewModel.selectCountry(item: item)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let item = viewModel.dataSource.value[indexPath.row]
        viewModel.deselectCountry(item: item)
    }
}

// MARK: UISearchBarDelegate
extension CountriesViewController: UISearchBarDelegate {
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        searchBar.becomeFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if let text = searchBar.searchTextField.text {
            viewModel.search(text: text)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let text = searchBar.searchTextField.text {
            viewModel.search(text: text)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
        viewModel.search(text: "")
    }
}

// MARK: Builder
extension CountriesViewController {
    public static func build(container: DIContainer, selectedCountries: [Country] = []) -> CountriesViewController {
        let viewController = CountriesViewController()
        let viewModel = CountriesViewModel(container: container)
        viewModel.selectedCountries.send(selectedCountries)
        viewController.viewModel = viewModel
        return viewController
    }
}
