//
//  SelectedCountriesViewController.swift
//  Contries
//
//  Created by Mohsen Moghimi on 1/20/1402 AP.
//

import UIKit

class SelectedCountriesViewController: UIViewController {
    var viewModel: SelectedCountriesViewModel!
    // MARK: UI Emlements
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: view.bounds)
        tableView.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        tableView.separatorStyle = .none
        tableView.separatorColor = .clear
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        return tableView
    }()
    
    // MARK: View lifcycles
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Home"
        view.backgroundColor = .primaryColor
        constraintTableView()
        dataSourceSubscription()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Select countries", style: .done, target: self, action: #selector(addTapped))
    }
    
    // MARK: Subscriptios
    private func dataSourceSubscription() {
        viewModel.selectedCountries
            .receive(on: DispatchQueue.main)
            .sink {[weak self] value in
                value.count == 0 ? self?.tableView.setEmptyMessage("No country selected!") : self?.tableView.restore()
                self?.tableView.reloadData()
        }.store(in: &viewModel.cancellables)
    }
    
    // MARK: Constraint views
    private func constraintTableView() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(
            equalTo: view.topAnchor,
            constant: 0
        ).isActive = true
        tableView.bottomAnchor.constraint(
            equalTo: view.bottomAnchor,
            constant: 0
        ).isActive = true
        tableView.leadingAnchor.constraint(
            equalTo: view.leadingAnchor,
            constant: 0
        ).isActive = true
        tableView.trailingAnchor.constraint(
            equalTo: view.trailingAnchor,
            constant: 0
        ).isActive = true
    }
    
    // MARK: Navigations
    @objc private func addTapped() {
        let viewController = CountriesViewController.build(
            container: viewModel.container,
            selectedCountries: viewModel.selectedCountries.value
        )
        viewController.viewModel.selectedCountries
            .receive(on: DispatchQueue.main)
            .sink {[weak self] data in
                self?.viewModel.selectedCountries.send(data)
        }.store(in: &viewModel.cancellables)
        navigationController?.pushViewController(viewController, animated: true)
    }
}

// MARK: UITableViewDelegate & UITableViewDataSource
extension SelectedCountriesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = viewModel.selectedCountries.value[indexPath.row]
        let cell = CountryTableViewCell()
        cell.config(item: item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.selectedCountries.value.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

// MARK: Builder
extension SelectedCountriesViewController {
    public static func build(container: DIContainer) -> SelectedCountriesViewController {
        let viewController = SelectedCountriesViewController()
        let viewModel = SelectedCountriesViewModel(container: container)
        viewController.viewModel = viewModel
        return viewController
    }
}
