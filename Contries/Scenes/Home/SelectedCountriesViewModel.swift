//
//  SelectedCountriesViewModel.swift
//  Contries
//
//  Created by Mohsen Moghimi on 1/21/1402 AP.
//

import Foundation
import Combine

final class SelectedCountriesViewModel {
    let container: DIContainer
    var cancellables = Set<AnyCancellable>()
    var selectedCountries = CurrentValueSubject<[Country], Never>([])
    
    init(container: DIContainer) {
        self.container = container
    }
}
