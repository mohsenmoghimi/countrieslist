//
//  CountryTableViewCell.swift
//  Contries
//
//  Created by Mohsen Moghimi on 1/21/1402 AP.
//

import UIKit

class CountryTableViewCell: UITableViewCell {
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 2
        return stackView
    }()
    
    private lazy var neumorphicView: NeumorphicView = {
        let neumorphicView = NeumorphicView()
        neumorphicView.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        return neumorphicView
    }()
    
    private lazy var countryNameLabel: UILabel = {
        let nameLabel = UILabel()
        nameLabel.textColor = .primaryColor
        nameLabel.font = UIFont.boldSystemFont(ofSize: 16)
        nameLabel.numberOfLines = 0
        return nameLabel
    }()
    
    private lazy var capitalNameLabel: UILabel = {
        let capitalLabel = UILabel()
        capitalLabel.textColor = .primaryColor
        capitalLabel.font = UIFont.boldSystemFont(ofSize: 14)
        return capitalLabel
    }()
    
    private lazy var regionLabel: UILabel = {
        let regionLabel = UILabel()
        regionLabel.textColor = .primaryColor
        regionLabel.font = UIFont.systemFont(ofSize: 12)
        return regionLabel
    }()
    
    private lazy var flagImage: UIImageView = {
        let imageView = UIImageView(frame: .init(x: 0, y: 0, width: 64, height: 64))
        imageView.contentMode = .scaleAspectFill
        imageView.alpha = 0.9
        imageView.tintColor = .lightGray
        return imageView
    }()
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                setCellSelected()
            } else {
                setCellDeselected()
            }
        }
    }
    
    func config(item: Country) {
        backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        countryNameLabel.text = item.name?.official ?? "N/A"
        capitalNameLabel.text = item.capital?[0] ?? "N/A"
        regionLabel.text = [item.region, item.subregion].compactMap { $0 }
        .joined(separator: ", ")
        flagImage.image = UIImage(systemName: "flag.fill")
        if let flag = item.flags?.png,
           let url = URL(string: flag) {
            flagImage.downloaded(from: url)
        }
    }
    
    private func constraintNeumorphicView() {
        contentView.addSubview(neumorphicView)
        neumorphicView.translatesAutoresizingMaskIntoConstraints = false
        neumorphicView.topAnchor.constraint(
            equalTo: contentView.topAnchor,
            constant: 12
        ).isActive = true
        neumorphicView.bottomAnchor.constraint(
            equalTo: contentView.bottomAnchor,
            constant: -12
        ).isActive = true
        neumorphicView.leadingAnchor.constraint(
            equalTo: contentView.leadingAnchor,
            constant: 12
        ).isActive = true
        neumorphicView.trailingAnchor.constraint(
            equalTo: contentView.trailingAnchor,
            constant: -12
        ).isActive = true
    }
    
    private func constraintStackView() {
        neumorphicView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.topAnchor.constraint(
            equalTo: neumorphicView.topAnchor,
            constant: 8
        ).isActive = true
        stackView.bottomAnchor.constraint(
            equalTo: neumorphicView.bottomAnchor,
            constant: -8
        ).isActive = true
        stackView.leadingAnchor.constraint(
            equalTo: neumorphicView.leadingAnchor,
            constant: 16
        ).isActive = true
        stackView.trailingAnchor.constraint(
            equalTo: flagImage.leadingAnchor,
            constant: -8
        ).isActive = true
    }
    
    private func constraintImageView() {
        neumorphicView.addSubview(flagImage)
        flagImage.translatesAutoresizingMaskIntoConstraints = false
        flagImage.centerYAnchor.constraint(
            equalTo: neumorphicView.centerYAnchor,
            constant: 0
        ).isActive = true
        flagImage.widthAnchor.constraint(
            equalToConstant: 64
        ).isActive = true
        flagImage.trailingAnchor.constraint(
            equalTo: neumorphicView.trailingAnchor,
            constant: -16
        ).isActive = true
        flagImage.heightAnchor.constraint(
            equalToConstant: 64
        ).isActive = true
    }
    
    private func papulateStackView() {
        stackView.addArrangedSubview(countryNameLabel)
        stackView.addArrangedSubview(capitalNameLabel)
        stackView.addArrangedSubview(regionLabel)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        constraintNeumorphicView()
        constraintImageView()
        constraintStackView()
        papulateStackView()
        flagImage.layer.masksToBounds = false
        flagImage.layer.cornerRadius = 8
        flagImage.clipsToBounds = true
        selectionStyle = .none
    }
    
    private func setCellSelected() {
        neumorphicView.backgroundColor = .primaryColor
        capitalNameLabel.textColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        countryNameLabel.textColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        regionLabel.textColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
    }
    
    private func setCellDeselected() {
        neumorphicView.backgroundColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1)
        capitalNameLabel.textColor = .primaryColor
        countryNameLabel.textColor = .primaryColor
        regionLabel.textColor = .primaryColor
    }
}

