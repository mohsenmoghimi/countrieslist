//
//  DIContainer.swift
//  Contries
//
//  Created by Mohsen Moghimi on 1/20/1402 AP.
//

import Foundation

struct DIContainer {
    let services: Services
}

extension DIContainer {
    struct Services {
        let countriesService: CountriesService
    }
}

extension DIContainer {
    struct RemoteRepositories {
        let countriesRemoteRepository: CountriesRemoteRepository
    }
}

