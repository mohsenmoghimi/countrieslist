//
//  AppEnvironment.swift
//  Contries
//
//  Created by Mohsen Moghimi on 1/20/1402 AP.
//

import Foundation
import Alamofire

struct AppEnvironment {
    let container: DIContainer
}

extension AppEnvironment {
    static func bootstrap() -> Self {
        let client = configureClient()
        let remote = configureRemoteRepositories(client: client)
        let services = configureServices(remote: remote)
        let container = DIContainer(services: services)
        return .init(container: container)
    }
    
    static func configureClient() -> ApiClient {
        return AlamofireApiClient(session: Session())
    }
    
    static func configureServices(remote: DIContainer.RemoteRepositories) -> DIContainer.Services {
        .init(
            countriesService: .init(remote: remote.countriesRemoteRepository))
    }
    
    private static func configureRemoteRepositories(client: ApiClient) -> DIContainer.RemoteRepositories {
        .init(
            countriesRemoteRepository: .init(client: client))
    }
}


